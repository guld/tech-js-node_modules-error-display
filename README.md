# error-display

[![source](https://img.shields.io/badge/source-bitbucket-blue.svg)](https://bitbucket.org/guld/tech-js-node_modules-error-display) [![issues](https://img.shields.io/badge/issues-bitbucket-yellow.svg)](https://bitbucket.org/guld/tech-js-node_modules-error-display/issues) [![documentation](https://img.shields.io/badge/docs-guld.tech-green.svg)](https://guld.tech/lib/error-display.html)

[![node package manager](https://img.shields.io/npm/v/error-display.svg)](https://www.npmjs.com/package/error-display) [![travis-ci](https://travis-ci.org/guldcoin/tech-js-node_modules-error-display.svg)](https://travis-ci.org/guldcoin/tech-js-node_modules-error-display?branch=guld) [![lgtm](https://img.shields.io/lgtm/grade/javascript/b/guld/tech-js-node_modules-error-display.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/guld/tech-js-node_modules-error-display/context:javascript) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-error-display/status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-error-display) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-error-display/dev-status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-error-display?type=dev)

Show or hide error html elements using JS.

### Install

##### Browser

```sh
curl https///bitbucket.org/guld/tech-js-node_modules-error-display/raw/guld/error-display.js -o error-display.js
```

### Usage

```
<html>
  <body>
    <script src="error-display.js"></script>
    <script>
      errorDisplay.setError('error message') // contents: "error message"
      errorDisplay.setError('error message 2') // contents: "error message 2error message"
      errorDisplay.unsetError('error message 2') // contents: "error message"
      errorDisplay.clearError() // contents: ""
    </script>
    <div id="err-div"></div>
  </body>
</html>
```

### License

MIT Copyright isysd
